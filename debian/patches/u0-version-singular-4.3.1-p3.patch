Bug: https://trac.sagemath.org/ticket/34851

From 42342b90043cd7cd493f294e99e98e892850f883 Mon Sep 17 00:00:00 2001
From: Matthias Koeppe <mkoeppe@math.ucdavis.edu>
Date: Wed, 14 Dec 2022 18:14:24 -0800
Subject: Apply
 https://github.com/archlinux/svntogit-community/blob/packages/sagemath/trunk/sagemath-singular-4.3.1.p3.patch

---
 src/sage/libs/singular/decl.pxd                            | 2 +-
 src/sage/rings/polynomial/multi_polynomial_libsingular.pyx | 4 ++--
 2 files changed, 3 insertions(+), 3 deletions(-)

From 6e03fbe316108c33f2a100ddfe21641b483de690 Mon Sep 17 00:00:00 2001
From: Antonio Rojas <arojas@archlinux.org>
Date: Tue, 20 Dec 2022 18:40:30 +0100
Subject: Singular supports larger exponents now, adapt tests

---
 src/sage/libs/singular/singular.pyx                        | 13 +++++--------
 src/sage/rings/polynomial/multi_polynomial_libsingular.pyx | 12 ++++++------
 src/sage/rings/polynomial/plural.pyx                       |  8 ++++----
 src/sage/structure/element.pyx                             |  4 ++--
 4 files changed, 17 insertions(+), 20 deletions(-)

From 267177af61d742d1571c28c49b9660720173cba7 Mon Sep 17 00:00:00 2001
From: Antonio Rojas <arojas@archlinux.org>
Date: Tue, 20 Dec 2022 18:45:04 +0100
Subject: Fix one more test

---
 src/sage/rings/polynomial/multi_polynomial_ideal.py | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

From d2b0016b838703dd77e5b110a4a27ccfba534d32 Mon Sep 17 00:00:00 2001
From: Antonio Rojas <arojas@archlinux.org>
Date: Wed, 4 Jan 2023 15:31:12 +0100
Subject: Account for different bahavior of pTakeOutComp and pTakeOutComp1

---
 src/sage/rings/polynomial/multi_polynomial_libsingular.pyx | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/sage/src/sage/libs/singular/decl.pxd
+++ b/sage/src/sage/libs/singular/decl.pxd
@@ -574,7 +574,7 @@
 
     # gets a component out of a polynomial vector
 
-    poly *pTakeOutComp1(poly **, int)
+    poly *pTakeOutComp(poly **, int)
 
     # deep copy p
 
--- a/sage/src/sage/rings/polynomial/multi_polynomial_libsingular.pyx
+++ b/sage/src/sage/rings/polynomial/multi_polynomial_libsingular.pyx
@@ -193,7 +193,7 @@
     p_IsUnit, p_IsOne, p_Series, p_Head, idInit, fast_map_common_subexp, id_Delete,
     p_IsHomogeneous, p_Homogen, p_Totaldegree,pLDeg1_Totaldegree, singclap_pdivide, singclap_factorize,
     idLift, IDELEMS, On, Off, SW_USE_CHINREM_GCD, SW_USE_EZGCD,
-    p_LmIsConstant, pTakeOutComp1, singclap_gcd, pp_Mult_qq, p_GetMaxExp,
+    p_LmIsConstant, pTakeOutComp, singclap_gcd, pp_Mult_qq, p_GetMaxExp,
     pLength, kNF, p_Neg, p_Minus_mm_Mult_qq, p_Plus_mm_Mult_qq,
     pDiff, singclap_resultant, p_Normalize,
     prCopyR, prCopyR_NoSort)
@@ -1631,7 +1631,7 @@
             9/4
 
             sage: P.monomial_quotient(x,y) # Note the wrong result
-            x*y^65535*z^65535
+            x*y^1048575*z^1048575
 
             sage: P.monomial_quotient(x,P(1))
             x
@@ -2292,7 +2292,7 @@
             9/4*x^2 - 1/4*y^2 - y - 1
 
             sage: P.<x,y> = PolynomialRing(QQ,order='lex')
-            sage: (x^2^15) * x^2^15
+            sage: (x^2^32) * x^2^32
             Traceback (most recent call last):
             ...
             OverflowError: exponent overflow (...)
@@ -2407,7 +2407,7 @@
             ValueError: not a 2nd power
 
             sage: P.<x,y> = PolynomialRing(QQ,order='lex')
-            sage: (x+y^2^15)^10
+            sage: (x+y^2^32)^10
             Traceback (most recent call last):
             ....
             OverflowError: exponent overflow (...)
@@ -3488,16 +3488,16 @@
         We are catching overflows::
 
             sage: R.<x,y> = QQ[]
-            sage: n=100; f = x^n
+            sage: n=10000; f = x^n
             sage: try:
             ....:     f.subs(x = x^n)
             ....:     print("no overflow")
             ....: except OverflowError:
             ....:     print("overflow")
-            x^10000
+            x^100000000
             no overflow
 
-            sage: n = 1000
+            sage: n = 100000
             sage: try:
             ....:     f = x^n
             ....:     f.subs(x = x^n)
@@ -4603,7 +4603,7 @@
         l = []
         for i from 0 <= i < IDELEMS(res):
             for j from 1 <= j <= IDELEMS(_I):
-                l.append( new_MP(parent, pTakeOutComp1(&res.m[i], j)) )
+                l.append( new_MP(parent, pTakeOutComp(&res.m[i], 1)) )
 
         id_Delete(&fI, r)
         id_Delete(&_I, r)
--- a/sage/src/sage/libs/singular/singular.pyx
+++ b/sage/src/sage/libs/singular/singular.pyx
@@ -1477,20 +1477,17 @@
 
     Whether an overflow occurs or not partially depends
     on the number of variables in the ring. See trac ticket
-    :trac:`11856`. With Singular 4, it is by default optimized
-    for at least 4 variables on 64-bit and 2 variables on 32-bit,
-    which in both cases makes a maximal default exponent of
-    2^16-1.
+    :trac:`11856`.
 
     EXAMPLES::
 
         sage: P.<x,y> = QQ[]
-        sage: y^(2^16-1)
-        y^65535
-        sage: y^2^16
+        sage: y^(2^30)
+        y^1073741824
+        sage: y^2^32
         Traceback (most recent call last):
         ...
-        OverflowError: exponent overflow (65536)
+        OverflowError: exponent overflow (4294967296)
     """
     if unlikely(e > _ring.bitmask):
         raise OverflowError("exponent overflow (%d)"%(e))
--- a/sage/src/sage/rings/polynomial/plural.pyx
+++ b/sage/src/sage/rings/polynomial/plural.pyx
@@ -1631,10 +1631,10 @@
             sage: P = A.g_algebra(relations={y*x:-x*y + z},  order='lex')
             sage: P.inject_variables()
             Defining x, z, y
-            sage: (x^2^15) * x^2^15
+            sage: (x^2^31) * x^2^31
             Traceback (most recent call last):
             ...
-            OverflowError: exponent overflow (65536)
+            OverflowError: exponent overflow (2147483648)
         """
         # all currently implemented rings are commutative
         cdef poly *_p
@@ -1701,10 +1701,10 @@
             sage: P = A.g_algebra(relations={y*x:-x*y + z},  order='lex')
             sage: P.inject_variables()
             Defining x, z, y
-            sage: (x+y^2^15)^10
+            sage: (x+y^2^31)^10
             Traceback (most recent call last):
             ....
-            OverflowError: exponent overflow (327680)
+            OverflowError: exponent overflow (2147483648)
         """
         if type(exp) is not Integer:
             try:
--- a/sage/src/sage/structure/element.pyx
+++ b/sage/src/sage/structure/element.pyx
@@ -2700,10 +2700,10 @@
         with Singular 4::
 
             sage: K.<x,y> = ZZ[]
-            sage: (x^12345)^54321
+            sage: (x^123456)^654321
             Traceback (most recent call last):
             ...
-            OverflowError: exponent overflow (670592745)
+            OverflowError: exponent overflow (80779853376)
         """
         return arith_generic_power(self, n)
 
--- a/sage/src/sage/rings/polynomial/multi_polynomial_ideal.py
+++ b/sage/src/sage/rings/polynomial/multi_polynomial_ideal.py
@@ -60,7 +60,7 @@
 Note that the result of a computation is not necessarily reduced::
 
     sage: (a+b)^17
-    256*a*b^16 + 256*b^17
+    a*b^16 + b^17
     sage: S(17) == 0
     True
 
